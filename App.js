/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState} from 'react';
import {Text, View, TouchableOpacity, StyleSheet} from 'react-native';

const App = () => {
  const [number, setNumber] = useState(0);

  function handleNumber() {
    const newNumber = Math.floor(Math.random() * 100);

    setNumber(newNumber);
  }

  return (
    <>
      <View style={styles.container}>
        <Text style={styles.number}>{number}</Text>
        <TouchableOpacity style={styles.button} onPress={handleNumber}>
          <Text>Gerar número</Text>
        </TouchableOpacity>
        <Text>Gera números aleatórios até 100</Text>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#e1e1e1',
    fontSize: 18,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  number: {
    fontSize: 52,
    color: '#FFFFFFFF',
    fontWeight: 'bold',
  },
  button: {
    backgroundColor: '#d6d6d6',
    width: '35%',
    padding: 10,
    margin: 10,
    marginBottom: 25,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default App;
